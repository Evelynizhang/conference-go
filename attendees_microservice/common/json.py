from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(
        self, o
    ):  # o is an instance of the class stored in the self.model
        # self is ModelEncoder
        #   if the object to decode is the same class as what's in the
        if isinstance(o, self.model):
            #   model property, then
            #     * create an empty dictionary that will hold the property names
            #       as keys and the property values as values
            dic = {}
            #     * for each name in the properties list
            if hasattr(o, "get_api_url"):
                dic["href"] = o.get_api_url()
            for property in self.properties:
                #         * get the value of that property from the model instance
                #           given just the property name
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                #         * put it into the dictionary with that property name as
                #           the key
                dic[property] = value
            #     * return the dictionary
            dic.update(self.get_extra_data(o))
            return dic
        #   otherwise,
        #       return super().default(o)  # From the documentation
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
